<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLdjawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ldjawaban', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('poin');
            $table->unsignedBigInteger('jawaban_id1');
            $table->foreign('jawaban_id1')->references('id')->on('jawaban');
            $table->unsignedBigInteger('users_id2');
            $table->foreign('users_id2')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ldjawaban');
    }
}
